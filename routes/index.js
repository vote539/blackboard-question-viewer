var fs = require("fs"),
	path = require("path"),
	unzip = require("unzip"),
	libxmljs = require("libxmljs"),
	randy = require("randy");

// Make sure that both these paths exist
var ZIP_DIR = "blackboard_question_pool_zips";
var XML_DIR = "private/question_xmls";

exports.index = function(req, res){
	fs.readdir(XML_DIR, function(err, files){
		if(err) console.warn(err);
		res.render("index", {
			title: "Blackboard Question Viewer",
			files: files
		});
	});
};

exports.questions = function(req, res){
	var filename = req.query.filename;
	fs.readFile(path.join(XML_DIR, filename), function(err, data){
		if(err) console.warn(err);
		var xmlRoot = libxmljs.parseXmlString(data);
		var i = 0;
		var items = xmlRoot.find("/questestinterop/assessment/section/item").map(function(item){
			var flows = item.find("presentation/flow/flow");
			var questionText = flows[0].text();
			var isFillInTheBlank=false, shuffleChoices=false;
			if (flows.length == 2) {
				if (flows[1].text() == "") {
					// fill in the blank
					isFillInTheBlank = true;
				} else {
					// multiple choice/answer, or truefalse
					var correctChoiceIds = item.get("resprocessing/respcondition[@title='correct']/conditionvar").find("varequal | and/varequal").map(function(varequal){
						return varequal.text();
					});
					var answerIdsAndTexts = flows[1].find("response_lid/render_choice/flow_label/response_label").map(function(response_label){
						shuffleChoices = (response_label.attr("shuffle").value() == "Yes");
						var id = response_label.attr("ident").value();
						return {
							id: id,
							text: response_label.text(),
							correct: correctChoiceIds.indexOf(id) > -1
						};
					});
					if (shuffleChoices) answerIdsAndTexts = randy.shuffle(answerIdsAndTexts);
				}
			} else if (flows.length == 3) {
				// matching
				var correctMatches = {};
				item.find("resprocessing/respcondition/conditionvar/varequal").forEach(function(varequal){
					correctMatches[varequal.attr("respident").value()] = varequal.text();
				});
				var answerIdsAndTexts = flows[1].find("flow").map(function(subflow){
					var id = subflow.get("response_lid").attr("ident").value();
					var matchesArray = subflow.find("response_lid/render_choice/flow_label/response_label").map(function(response_label){
						return response_label.attr("ident").value();
					});
					return {
						id: id,
						text: subflow.text(),
						matchIndex: matchesArray.indexOf(correctMatches[id])
					};
				});
				var matchTexts = flows[2].find("flow").map(function(subflow){
					return {
						text: subflow.text()
					};
				});
			}
			return {
				question: questionText,
				choices: answerIdsAndTexts,
				isFillInTheBlank: isFillInTheBlank,
				matchingChoices: matchTexts,
				shortId: ++i
			};
		});

		res.render("questions", {
			items: items,
			    filename: filename
		});
	});
};

exports.unzip = function(req, res){
	fs.readdir(ZIP_DIR, function(err, files){
		if (err) {
			res.send(err);
			return;
		};
		files.forEach(function(file){
			fs.createReadStream(path.join(ZIP_DIR, file)).pipe(unzip.Parse()).on("entry", function(entry){
				if (entry.path == "res00001.dat") {
					// res00001.dat is the file in Blackboard downloads that contains the actual question data
					var xmlFilename = path.join(XML_DIR, path.basename(file, ".zip")+".xml");
					entry.pipe(fs.createWriteStream(xmlFilename));
				} else {
					entry.autodrain();
				}
			});
		});
		res.redirect("/");
	});
};