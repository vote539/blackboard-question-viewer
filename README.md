Blackboard Question Viewer
==========================

This is a simple application that enables you to browse the questions from a question pool export file that you download from the Blackboard course management system, without needing to upload the questions back to Blackboard again.

## Installation

You need to [install the Node.JS interpreter on your machine](http://howtonode.org/how-to-install-nodejs).  There are millions of tutorials on how to do this.

Once Node.JS is installed, choose a directory where you want to save the application files.  Open a terminal and run the following commands:

    cd /path/to/destination
    git clone git@bitbucket.org:vote539/blackboard-question-viewer.git
    cd blackboard-question-viewer
    mkdir blackboard_question_pool_zips
    mkdir private
    mkdir private/question_xmls
    npm install
    node app.js

## Usage

To launch the application, point your web browser to [http://localhost:6789/](http://localhost:6789/).

Save files that you download from Blackboard inside the `blackboard_question_pool_zips` directory in the application.  Then, open the application in your web browser and press the "extract" link.  Reload the page, and your questions will be ready for viewing!

## License

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
