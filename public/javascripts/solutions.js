var solutionsShowing = false;
function toggleSolutions(){
	solutionsShowing = !solutionsShowing;

	document.body.className = solutionsShowing ? "reveal" : "";

	Array.prototype.slice.call(document.getElementsByClassName("match-solution")).forEach(function(span){
		span.parentNode.removeChild(span);
	});

	if(solutionsShowing) Array.prototype.slice.call(document.querySelectorAll("ol.mc > li:not([data-match-index='undefined'])"), 0).forEach(function(li){
		var solutionObj = document.createElement("span");
		solutionObj.className = "match-solution";
		var matchNum = (parseInt(li.getAttribute("data-match-index"))+1);
		if (matchNum) solutionObj.appendChild(document.createTextNode("Match #" + matchNum));
		li.appendChild(solutionObj);
	});
};